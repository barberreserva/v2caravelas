﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaDesktop
{
    /// <summary>
    /// Lógica interna para FoneCliente.xaml
    /// </summary>
    public partial class FoneCliente : Window
    {
        public string codigoCli;
        public string codigoFone;
        public FoneCliente()
        {
            InitializeComponent();
            cmbNome.Focus();
        }

        private void Fechar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Cliente frm = new Cliente();
            frm.Show();
            Close();
        }
        private void Limpar()
        {
            txtFoneCliente.Clear();
            cmbTipo.Text = "";
            cmbDescricao.Text = "";
            txtFoneCliente.IsEnabled = false;
            cmbTipo.IsEnabled = false;
            cmbDescricao.IsEnabled = false;
            btnFoneAdd.IsEnabled = false;
        }

        private void BtnFoneAdd_Click(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string inserir = "INSERT INTO foneCli(numeroFone,tipoFone,descFone,idcliente)VALUES('" + txtFoneCliente.Text + "','" + cmbTipo.Text + "','" + cmbDescricao.Text + "','" + codigoCli + "')'";
            MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
            comandos.ExecuteNonQuery();

            bd.Desconectar();
            MessageBox.Show("Telefone cadastrado com sucesso!!!", "Cadastro de Telefones");
            Limpar();
            txtFoneCliente.IsEnabled = true;
            txtFoneCliente.Focus();
        }

        private void FoneCliente1_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT idcliente, nomeCli FROM cliente";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cmbNome.DisplayMemberPath = "nomeCli";
            cmbNome.ItemsSource = dt.DefaultView;
        }

        private void BtnFoneAlterar_Click(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();
            string alterar = "UPDATE fonecli SET" + "numeroFone='" + txtFoneCliente.Text + "'," + "tipoFone='" + cmbTipo.Text + "'," + "descFone'" + cmbDescricao.Text + "' " + "WHERE idfoneCli='" + codigoFone + "'";
            MySqlCommand comandos = new MySqlCommand(alterar, bd.conexao);
            comandos.ExecuteNonQuery();
            bd.Desconectar();
            MessageBox.Show("Telefone alterado com sucesso!!!", "Alteração de telefones");

            Cliente frm = new Cliente();
            frm.Show();
            Close();
        }

        private void CmbNome_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (cmbNome.Text != "")
            {
                Banco bd = new Banco();
                bd.Conectar();
                string selecionar = "SELECT * FROM cliente WHERE nomeCli = ?";
                MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
                com.Parameters.Clear();
                com.Parameters.Add("@nomeCli", MySqlDbType.String).Value = cmbNome.Text;
                com.CommandType = CommandType.Text;
                MySqlDataReader dr = com.ExecuteReader();
                dr.Read();
                codigoCli = dr.GetString(0);
                txtFoneCliente.IsEnabled = true;
                txtFoneCliente.Focus();
            }
        }

        private void TxtFoneCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                cmbTipo.IsEnabled = true;
                cmbTipo.Focus();
            }
        }

        private void CmbTipo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbDescricao.IsEnabled = true;
            cmbDescricao.Focus();
        }

        private void CmbDescricao_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnFoneAdd.IsEnabled = true;
            btnFoneAdd.Focus();
        }
    }
}
