﻿#pragma checksum "..\..\Servicos.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "04E20BB08F3C389C612007BEB0A938E6AEEADAC9E39FCA317F50DEB668A06FE9"
//------------------------------------------------------------------------------
// <auto-generated>
//     O código foi gerado por uma ferramenta.
//     Versão de Tempo de Execução:4.0.30319.42000
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------

using SistemaDesktop;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SistemaDesktop {
    
    
    /// <summary>
    /// Servicos
    /// </summary>
    public partial class Servicos : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 1 "..\..\Servicos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SistemaDesktop.Servicos Servicos1;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\Servicos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas tituloCliente;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\Servicos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnServicoAdd;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\Servicos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnServicoAlterar;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\Servicos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBuscarServico;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\Servicos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox boxTipoBuscaServico;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\Servicos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dataGridServico;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\Servicos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle FecharServico;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SistemaDesktop;component/servicos.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Servicos.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Servicos1 = ((SistemaDesktop.Servicos)(target));
            return;
            case 2:
            this.tituloCliente = ((System.Windows.Controls.Canvas)(target));
            return;
            case 3:
            this.btnServicoAdd = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.btnServicoAlterar = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\Servicos.xaml"
            this.btnServicoAlterar.Click += new System.Windows.RoutedEventHandler(this.BtnServicoAlterar_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.txtBuscarServico = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.boxTipoBuscaServico = ((System.Windows.Controls.ComboBox)(target));
            
            #line 22 "..\..\Servicos.xaml"
            this.boxTipoBuscaServico.IsMouseCapturedChanged += new System.Windows.DependencyPropertyChangedEventHandler(this.BoxTipoBuscaServico_IsMouseCapturedChanged);
            
            #line default
            #line hidden
            return;
            case 7:
            this.dataGridServico = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 8:
            this.FecharServico = ((System.Windows.Shapes.Rectangle)(target));
            
            #line 24 "..\..\Servicos.xaml"
            this.FecharServico.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.FecharServico_MouseDown);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

