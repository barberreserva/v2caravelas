﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaDesktop
{
    /// <summary>
    /// Lógica interna para servicoCadastro.xaml
    /// </summary>
    public partial class servicoCadastro : Window
    {

        public string codigoEmpresa, empresa;
        public double valor;
        string arquivo1, arquivo2, arquivo3;

        

        public servicoCadastro()
        {
            InitializeComponent();
            txtServNome.Focus();
            txtServNome.SelectAll();

            System.Text.RegularExpressions.Regex num = new System.Text.RegularExpressions.Regex("[^0-9]");
            //se for número
            if (!num.IsMatch(this.txtServCodigo.Text))
            {
                txtServValor.IsEnabled = true;
                chkStatus.IsEnabled = true;
                dataServCad.IsEnabled = true;
                btnFoto1.IsEnabled = true;
                btnFoto2.IsEnabled = true;
                btnFoto3.IsEnabled = true;
                txtServDescricao.IsEnabled = true;
                txtServDuracao.IsEnabled = true;
                cmbServEmpresa.IsEnabled = true;

                btnServAlterar.IsEnabled = true;
                btnServAdicionar.IsEnabled = false;
            }
        }
        private void BtnFoto1_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            dlg.InitialDirectory = "c:\\";
            dlg.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                arquivo1 = dlg.FileName;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(arquivo1);
                bitmap.EndInit();

                int foto1 = arquivo1.IndexOf("servico");
                string nomefoto1 = arquivo1.Substring(foto1 + 8);
                txtServFoto1.Text = nomefoto1;

                btnFoto2.IsEnabled = true;
            }
        }
    }
}
