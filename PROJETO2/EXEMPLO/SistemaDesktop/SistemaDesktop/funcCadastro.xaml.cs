﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaDesktop
{
    /// <summary>
    /// Lógica interna para funcCadastro.xaml
    /// </summary>
    public partial class funcCadastro : Window
    {
        public string empresa, codempresa;

       

        public funcCadastro()
        {
            InitializeComponent();

            txtFuncNome.Focus();
            txtFuncNome.SelectAll();
        }

        

        private void FecharLogin_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Funcionarios frm = new Funcionarios();
            frm.Show();
            Close();
        }

        private void TxtFuncEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtFuncEmail.Text != "")
                {
                    txtFuncSenha.IsEnabled = true;
                    txtFuncSenha.Focus();
                    txtFuncSenha.SelectAll();
                }
                else
                {
                    MessageBox.Show("Preencher o E-Mail");
                    txtFuncEmail.Focus();
                }
            }
        }

        private void TxtFuncSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtFuncSenha.Text != "")
                {
                    cmbFuncNivel.IsEnabled = true;

                    ChkStatus.IsEnabled = true;
                    MessageBox.Show("Não esqueça do **STATUS** do Funcionário");
                    dataFuncCad.IsEnabled = true;
                    dataFuncCad.Focus();

                    System.Text.RegularExpressions.Regex num = new System.Text.RegularExpressions.Regex("[^0-9]");
                    
                    if (num.IsMatch(this.txtFuncCodigo.Text))
                    {
                        btnFuncAdd.IsEnabled = true;
                    }

                }
                else
                {
                    MessageBox.Show("Preencher a senha");
                    txtFuncSenha.Focus();
                }

            }
        }

        private void BtnFuncAdd_Click(object sender, RoutedEventArgs e)
        {
            string status;
            DateTime data = dataFuncCad.DisplayDate;

            Banco bd = new Banco();
            bd.Conectar();
            if (ChkStatus.IsChecked == true)
            {
                status = "ATIVO";
                string inserir = "INSERT INTO funcionario(numeroFunc,emailFunc,senhaFunc,nivelFunc,statusFunc,dataCadFunc,horarioTrabalho,idempresa)VALUES('" + txtFuncNome.Text + "','" + txtFuncEmail.Text + "','" + txtFuncSenha.Text + "','" + cmbFuncNivel.Text +  "','" + status + "','" + data.ToString("yyyy-MM-dd") + "','" + cmbFuncCargaH.Text + "','" + codempresa + "')";
                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();

            }
            else
            {
                status = "INATIVO";
                string inserir = "INSERT INTO funcionario(numeroFunc,emailFunc,senhaFunc,nivelFunc,statusFunc,dataCadFunc,horarioTrabalho,idempresa)VALUES('" + txtFuncNome.Text + "','" + txtFuncEmail.Text + "','" + txtFuncSenha.Text + "','" + cmbFuncNivel.Text + "','" + status + "','" + data.ToString("yyyy-MM-dd") + "','" + cmbFuncCargaH.Text + "','" + codempresa + "')";
                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            bd.Desconectar();
            MessageBox.Show("Funcionario cadastrado com sucesso!!!", "Cadastro de Funcionarios");
        }

        private void BtnFuncFoneAdd_Click(object sender, RoutedEventArgs e)
        {
            FoneCliente btnFoneAdd = new FoneCliente();
            btnFoneAdd.Show();
        }

        private void FuncCadastro1_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT * FROM empresa ORDER BT nomeEmp";

            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cmbFuncEmpresa.DisplayMemberPath = "nomeEmp";
            cmbFuncEmpresa.ItemsSource = dt.DefaultView;
        }

        private void CmbFuncEmpresa_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (cmbFuncEmpresa.Text != "")
            {
                Banco bd = new Banco();
                bd.Conectar();
                string selecionar = "SELECT idempresa, nomeEmp FROM empresa WHERE nomeEmp = ?";
                MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
                com.Parameters.Clear();
                com.Parameters.Add("@nomeEmp", MySqlDbType.String).Value = cmbFuncEmpresa.Text;
                com.CommandType = CommandType.Text;
                MySqlDataReader dr = com.ExecuteReader();
                dr.Read();
                codempresa = dr.GetString(0);
               
            }
        }

        private void BtnFuncFoneAlterar_Click(object sender, RoutedEventArgs e)
        {
            string status;
            DateTime data = dataFuncCad.DisplayDate;

            Banco bd = new Banco();
            bd.Conectar();
            if (ChkStatus.IsChecked == true)
            {
                status = "ATIVO";
                string alterar = "UPDATE funcionario SET " + "nomeFunc='" + txtFuncNome.Text + "'," + "emailFunc='" + txtFuncEmail.Text + "'," + "senhaFunc='" + txtFuncSenha.Text + "'," + "statusFunc='" + status + "'," + "dataCadFunc='" + data.ToString("yyyy-MM-dd") + "'," + "horarioTrabalho='" + cmbFuncCargaH.Text + "'," + "idempresa='" + codempresa + "'" + "WHERE idfuncionario='" + txtFuncCodigo.Text + "'";
                MySqlCommand comandos = new MySqlCommand(alterar, bd.conexao);
                comandos.ExecuteNonQuery();


            }
            else
            {
                status = "INATIVO";
                string alterar = "UPDATE funcionario SET " + "nomeFunc='" + txtFuncNome.Text + "'," + "emailFunc='" + txtFuncEmail.Text + "'," + "senhaFunc='" + txtFuncSenha.Text + "'," + "statusFunc='" + status + "'," + "dataCadFunc='" + data.ToString("yyyy-MM-dd") + "'," + "horarioTrabalho='" + cmbFuncCargaH.Text + "'," + "idempresa='" + codempresa + "'" + "WHERE idfuncionario='" + txtFuncCodigo.Text + "'";
                MySqlCommand comandos = new MySqlCommand(alterar, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            bd.Desconectar();
            MessageBox.Show("Funcionario alterado com sucesso!!!", "Alteração de Funcionarios");
        }

        private void TxtFuncNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtFuncNome.Text != "")
                {
                    txtFuncEmail.IsEnabled = true;
                    txtFuncEmail.Focus();
                    txtFuncEmail.SelectAll();
                }
                else
                {
                    MessageBox.Show("Preencher o nome");
                    txtFuncNome.Focus();
                }
            }
        }


    }
}
