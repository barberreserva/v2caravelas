﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaDesktop
{
    /// <summary>
    /// Lógica interna para Servicos.xaml
    /// </summary>
    public partial class Servicos : Window
    {
        public string foto1, foto2, foto3;
        public Servicos()
        {
            InitializeComponent();
        }

        private void BtnServicoAlterar_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridServico.SelectedIndex >= 0)
            {
                var rowView = dataGridServico.SelectedItems[0] as DataRowView;

                Hide();
                servicoCadastro alterar = new servicoCadastro();
                alterar.Show();
                alterar.txtServCodigo.Text = rowView["idservico"].ToString();
                alterar.txtServNome.Text = rowView["nomeServico"].ToString();
                alterar.txtServValor.Text = rowView["valorServico"].ToString();
                alterar.dataServCad.Text = rowView["dataCadServico"].ToString();
                if (rowView["statusServico"].ToString() == "ATIVO")
                {
                    alterar.chkStatus.IsChecked = true;
                }
                else
                {
                    alterar.chkStatus.IsChecked = false;
                }
                foto1 = rowView["fotoServico1"].ToString();
                if (foto1 != "")
                {
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = new Uri(foto1);
                    bitmap.EndInit();
                    alterar.imgFoto1.Source = bitmap;
                    alterar.btnFoto1.IsEnabled = true;
                }
                foto2 = rowView["fotoServico2"].ToString();
                if (foto2 != "")
                {
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = new Uri(foto2);
                    bitmap.EndInit();
                    alterar.imgFoto2.Source = bitmap;
                    alterar.btnFoto2.IsEnabled = true;
                }
                foto3 = rowView["fotoServico3"].ToString();
                if (foto3 != "")
                {
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = new Uri(foto3);
                    bitmap.EndInit();
                    alterar.imgFoto3.Source = bitmap;
                    alterar.btnFoto3.IsEnabled = true;
                }
                alterar.txtServDescricao.Text = rowView["descServico"].ToString();
                alterar.txtServDuracao.Text = rowView["tempoServico"].ToString();
                alterar.codigoEmpresa = rowView["idempresa"].ToString();

                //buscar o nome da empresa pelo código que consta no grid
                Banco bd = new Banco();
                bd.Conectar();

                string selecionar = "SELECT nomeEmp FROM empresa WHERE idempresa = ?";
                MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
                com.Parameters.Clear();
                com.Parameters.Add("@idempresa", MySqlDbType.String).Value = alterar.codigoEmpresa;
                com.CommandType = CommandType.Text; /* Executa o comando */
                                                    //recebe conteúdo do banco
                MySqlDataReader dr = com.ExecuteReader();
                dr.Read();
                alterar.cmbServEmpresa.Text = dr.GetString(0);

                alterar.txtServNome.IsEnabled = true;
                alterar.txtServValor.IsEnabled = true;
                alterar.chkStatus.IsEnabled = true;
                alterar.txtServDuracao.IsEnabled = true;
                alterar.dataServCad.IsEnabled = true;
                alterar.txtServDescricao.IsEnabled = true;
                alterar.cmbServEmpresa.IsEnabled = true;
                alterar.btnServAlterar.IsEnabled = true;
            }
        }

        private void FecharServico_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Kibeleza frm = new Kibeleza();
            frm.Show();
            Close();
        }
        private void BoxTipoBuscaServico_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT * FROM servico WHERE statusServico = @statusServico ORDER BY nomeServico ";

            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            com.Parameters.Clear();
            com.Parameters.Add("@statusServico", MySqlDbType.String).Value = boxTipoBuscaServico.Text;
            com.CommandType = CommandType.Text; /* Executa o comando */

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridServico.DisplayMemberPath = "nomeServico";
            dataGridServico.ItemsSource = dt.DefaultView;
        }
    }
}
