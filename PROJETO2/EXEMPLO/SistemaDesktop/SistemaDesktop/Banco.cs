﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SistemaDesktop
{
    public class Banco //mudar a classe para pública
    {

        string caminho = "SERVER=localhost;USER=root;DATABASE=kibeleza_novo";
        public MySqlConnection conexao; //instalar a referência MySQL.Data e usar a diretiva using

        public void Conectar()
        {
            try
            {
                conexao = new MySqlConnection(caminho);
                conexao.Open();
            }
            catch
            {
                MessageBox.Show("Erro ao tentar estabelecer conexão com o Banco de Dados", "ERRO!"); //usar a diretiva using
            }
        }

        public void Desconectar()
        {
            try
            {
                conexao = new MySqlConnection(caminho);
                conexao.Close();
            }
            catch
            {
                MessageBox.Show("Erro ao tentar se comunicar com o Banco de Dados", "ERRO!"); //usar a diretiva using
            }
        }

    }
}
