﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaDesktop
{
    /// <summary>
    /// Lógica interna para Funcionarios.xaml
    /// </summary>
    public partial class Funcionarios : Window
    {
        public Funcionarios()
        {
            InitializeComponent();
        }

        private void BtnFuncAdd_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            funcCadastro btnCadastro = new funcCadastro();
            btnCadastro.Show();
        }

        private void FecharFunc_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Kibeleza frm = new Kibeleza();
            frm.Show();
            Close();
        }

        private void CmbStatus_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT * FROM funcionario WHERE statusFunc = @statusFunc ORDER BY nomeFunc ";

            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            com.Parameters.Clear();
            com.Parameters.Add("@statusFunc", MySqlDbType.String).Value = cmbStatus.Text;
            com.CommandType = CommandType.Text; /* Executa o comando */

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridFunc.DisplayMemberPath = "nomeFunc";
            dataGridFunc.ItemsSource = dt.DefaultView;
        }

        private void TxtBuscarFunc_TextChanged(object sender, TextChangedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT * FROM funcionario WHERE nomeFunc = @nomeFunc ORDER BY nomeFunc ";

            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            com.Parameters.Clear();
            com.Parameters.Add("@nomeFunc", MySqlDbType.String).Value = txtBuscarFunc.Text;
            com.CommandType = CommandType.Text; /* Executa o comando */

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridFunc.DisplayMemberPath = "nomeFunc";
            dataGridFunc.ItemsSource = dt.DefaultView;
        }

        private void BtnFuncAlterar_Click(object sender, RoutedEventArgs e)
        {
            var rowView = dataGridFunc.SelectedItems[0] as DataRowView;
            Hide();
            funcCadastro alterar = new funcCadastro();
            alterar.Show();
            alterar.txtFuncCodigo.Text = rowView["idFuncionario"].ToString();
            alterar.txtFuncNome.Text = rowView["nomeFunc"].ToString();
            alterar.txtFuncEmail.Text = rowView["emailFunc"].ToString();
            alterar.txtFuncSenha.Text = rowView["senhaFunc"].ToString();
            alterar.cmbFuncNivel.Text = rowView["nivelFunc"].ToString();
            if (rowView["statusFunc"].ToString() == "ATIVO")
            {
                alterar.ChkStatus.IsChecked = true;
            }
            else
            {
                alterar.ChkStatus.IsChecked = false;
            }

            alterar.dataFuncCad.Text = rowView["dataCadFunc"].ToString();
            alterar.cmbFuncCargaH.Text = rowView["horarioTrabalho"].ToString();
            alterar.codempresa = rowView["idempresa"].ToString();

            //buscar o nome da empresa pelo código que consta na grid
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT nomeEmp FROM empresa WHERE idempresa = ?";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            com.Parameters.Clear();
            com.Parameters.Add("@idempresa", MySqlDbType.String).Value = alterar.codempresa;
            com.CommandType = CommandType.Text; /*EXECUTA O COMANDO */
                                                //RECEBE CONTEÙDO DO BANCO
            MySqlDataReader dr = com.ExecuteReader();
            dr.Read();
            alterar.cmbFuncEmpresa.Text = dr.GetString(0);
        }

        private void Funcionarios1_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT * FROM funcionario ORDER BY nomeFunc";

            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridFunc.DisplayMemberPath = "nomeFunc";
            dataGridFunc.ItemsSource = dt.DefaultView;
        }
    }
}
