﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;


namespace SistemaDesktop
{
    /// <summary>
    /// Lógica interna para Cliente.xaml
    /// </summary>
    public partial class Cliente : Window
    {        

    public Cliente()
        {
            InitializeComponent();            
        }

        private void BtnCliAdd_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            cliCadastro btnCadastro = new cliCadastro();
            btnCadastro.Show();
        }

        private void FecharCliente_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Kibeleza frm = new Kibeleza();
            frm.Show();
            Close();
        }

        private void Cliente1_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT * FROM cliente ORDER BY nomeCli";

            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridCliente.DisplayMemberPath = "nomeCli";
            dataGridCliente.ItemsSource = dt.DefaultView;
        }

        private void CmbStatus_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT * FROM cliente WHERE statusCli = @statusCli ORDER BY nomeCli ";

            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            com.Parameters.Clear();
            com.Parameters.Add("@statusCli", MySqlDbType.String).Value = cmbStatus.Text;
            com.CommandType = CommandType.Text; /* Executa o comando */

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridCliente.DisplayMemberPath = "nomeCli";
            dataGridCliente.ItemsSource = dt.DefaultView;
        }

        private void TxtBuscarCliente_TextChanged(object sender, TextChangedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT * FROM cliente WHERE nomeCli = @nomeCli ORDER BY nomeCli";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            com.Parameters.Clear();
            com.Parameters.Add("@nomeCli", MySqlDbType.String).Value = txtBuscarCliente.Text;
            com.CommandType = CommandType.Text; /*Executa o comando*/

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);

            dataGridCliente.DisplayMemberPath = "nomeCli";
            dataGridCliente.ItemsSource = dt.DefaultView;
        }
    }
}
