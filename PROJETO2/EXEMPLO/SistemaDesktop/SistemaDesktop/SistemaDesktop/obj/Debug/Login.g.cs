﻿#pragma checksum "..\..\Login.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "E23BF62AED14DCE4DA722A4B14760628488FC2FF"
//------------------------------------------------------------------------------
// <auto-generated>
//     O código foi gerado por uma ferramenta.
//     Versão de Tempo de Execução:4.0.30319.42000
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------

using SistemaDesktop;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SistemaDesktop {
    
    
    /// <summary>
    /// login
    /// </summary>
    public partial class login : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 1 "..\..\Login.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SistemaDesktop.login login1;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\Login.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle FecharLogin;
        
        #line default
        #line hidden
        
        
        #line 153 "..\..\Login.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle IconeUsuario;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\Login.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtUsuario;
        
        #line default
        #line hidden
        
        
        #line 155 "..\..\Login.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle IconeSenha;
        
        #line default
        #line hidden
        
        
        #line 156 "..\..\Login.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox TxtSenha;
        
        #line default
        #line hidden
        
        
        #line 157 "..\..\Login.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnEntrar;
        
        #line default
        #line hidden
        
        
        #line 158 "..\..\Login.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblTitulo;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\Login.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas Borboleta;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SistemaDesktop;component/login.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Login.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.login1 = ((SistemaDesktop.login)(target));
            return;
            case 2:
            this.FecharLogin = ((System.Windows.Shapes.Rectangle)(target));
            
            #line 148 "..\..\Login.xaml"
            this.FecharLogin.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.BtnFecharLogin_MouseDown);
            
            #line default
            #line hidden
            return;
            case 3:
            this.IconeUsuario = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 4:
            this.TxtUsuario = ((System.Windows.Controls.TextBox)(target));
            
            #line 154 "..\..\Login.xaml"
            this.TxtUsuario.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxtUsuario_KeyDown);
            
            #line default
            #line hidden
            return;
            case 5:
            this.IconeSenha = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 6:
            this.TxtSenha = ((System.Windows.Controls.PasswordBox)(target));
            
            #line 156 "..\..\Login.xaml"
            this.TxtSenha.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxtSenha_KeyDown);
            
            #line default
            #line hidden
            return;
            case 7:
            this.BtnEntrar = ((System.Windows.Controls.Button)(target));
            
            #line 157 "..\..\Login.xaml"
            this.BtnEntrar.Click += new System.Windows.RoutedEventHandler(this.btnEntrar_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.LblTitulo = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.Borboleta = ((System.Windows.Controls.Canvas)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

