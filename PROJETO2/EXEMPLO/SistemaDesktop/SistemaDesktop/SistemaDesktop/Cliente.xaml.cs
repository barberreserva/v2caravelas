﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;


namespace SistemaDesktop
{
    /// <summary>
    /// Lógica interna para Cliente.xaml
    /// </summary>
    public partial class Cliente : Window
    {
        public string[] tiposBox { get; set; }
        

    public Cliente()
        {
            InitializeComponent();
            tiposBox = new string[] { "Tipo 01", "Tipo 02", "Tipo 03", "Tipo 04" };
            DataContext = this;
        }

        private void BtnCliAdd_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            cliCadastro btnCadastro = new cliCadastro();
            btnCadastro.Show();
        }

        private void FecharCliente_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Kibeleza frm = new Kibeleza();
            frm.Show();
            Close();
        }
    }
}
