﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SistemaDesktop
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class Kibeleza : Window
    {
        public Kibeleza()
        {
            InitializeComponent();
        }

        private void btnCliente_Click(object sender, MouseButtonEventArgs e)
        {
            Hide();
            Cliente janelaCliente = new Cliente();
            janelaCliente.Show();           
        }

        private void BtnFecharSistema(object sender, MouseButtonEventArgs e)
        {
            var sair = MessageBox.Show(
                "Tem certeza que deseja ENCERRAR o sitema?",
                "Encerrar Sistema",
                MessageBoxButton.YesNo,
                MessageBoxImage.Exclamation);

            if (sair != MessageBoxResult.Yes)
            {
                return;
            }
            else
            {
                //Encerrar a aplicação corrente
                Application.Current.Shutdown();
            }
        }
    }
}
