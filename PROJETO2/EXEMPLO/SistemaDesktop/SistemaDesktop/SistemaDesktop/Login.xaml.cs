﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace SistemaDesktop
{
    /// <summary>
    /// Lógica interna para login.xaml
    /// </summary>
    public partial class login : Window
    {
        public string usuario, senha;

        public login()
        {
            InitializeComponent();
            TxtUsuario.Focus();
            TxtUsuario.SelectAll();
        }
        
        private void BtnFecharLogin_MouseDown(object sender, MouseButtonEventArgs e)
        {            
            var sair = MessageBox.Show(
                "Tem certeza que deseja ENCERRAR o sitema?",
                "Encerrar Sistema",
                MessageBoxButton.YesNo,
                MessageBoxImage.Exclamation);

            if (sair != MessageBoxResult.Yes)
            {
                return;
            }
            else
            {
                //Encerrar a aplicação corrente
                Application.Current.Shutdown();
            }
        }

        private void TxtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (TxtUsuario.Text == "")
                {
                    MessageBox.Show("Preencher o campo usuário corretamente!!!", "Login do Usuário");
                }
                else
                { 
                    usuario = TxtUsuario.Text;
                    TxtSenha.IsEnabled = true;
                    TxtSenha.Focus();                    
                }
            }
        }

        private void TxtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (TxtSenha.Password == "")
                {
                    MessageBox.Show("Preencher o campo senha corretamente!!!", "Senha do Usuário");
                }
                else
                {
                    senha = TxtSenha.Password;
                    BtnEntrar.IsEnabled = true;
                    BtnEntrar.Focus();
                }
            }
        }

        private void btnEntrar_Click(object sender, RoutedEventArgs e)
        {
            if (usuario == "ADMIN" && senha == "1234")
            {
                Hide(); //Esconder
                Kibeleza frm = new Kibeleza();
                frm.Show();
            }
            else if (usuario == "TESTE" && senha == "5678")
            {
                Hide();
                Kibeleza frm = new Kibeleza();
                frm.Show();
                frm.btnCliente.IsEnabled = false;
                frm.btnFunc.IsEnabled = false;
            }
            else
            {
                MessageBox.Show("Favor preencher o usuário ou a senha corretamente!!!");
                TxtUsuario.Clear();
                TxtSenha.Clear();
                TxtUsuario.Focus();
                TxtSenha.IsEnabled = false;
                BtnEntrar.IsEnabled = false;
            }
        }

    }
}