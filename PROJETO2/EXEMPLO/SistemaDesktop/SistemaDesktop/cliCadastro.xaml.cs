﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SistemaDesktop
{
    /// <summary>
    /// Lógica interna para cliCadastro.xaml
    /// </summary>
    public partial class cliCadastro : Window
    {
        string arquivoSelecionado;

        public cliCadastro()
        {

            InitializeComponent();
            txtCliNome.Focus();
            txtCliNome.SelectAll();
        }

        private void BtnCliFoneAdd_Click(object sender, RoutedEventArgs e)
        {
            FoneCliente btnFoneAdd = new FoneCliente();
            btnFoneAdd.Show();
        }

        private void FecharLogin_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Cliente frm = new Cliente();
            frm.Show();
            Close();
        }

        private void TxtCliNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtCliNome.Text != "")
                {
                    txtCliEmail.IsEnabled = true;
                    txtCliEmail.Focus();
                    txtCliEmail.SelectAll();
                }
                else
                {
                    MessageBox.Show("Preencher o nome");
                    txtCliNome.Focus();
                }
            }
        }

        private void TxtCliEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtCliEmail.Text != "")
                {
                    txtCliSenha.IsEnabled = true;
                    txtCliSenha.Focus();
                    txtCliSenha.SelectAll();
                }
                else
                {
                    MessageBox.Show("Preencher o E-Mail");
                    txtCliEmail.Focus();
                }
            }
        }

        private void TxtCliSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtCliSenha.Text != "")
                {
                    ChkStatus.IsEnabled = true;
                    MessageBox.Show("Não esqueça do **STATUS** do Cliente");
                    dataCliCad.IsEnabled = true;
                    dataCliCad.Focus();
                    btnFoto.IsEnabled = true;
                    btnCliAdd.IsEnabled = true;
                }
                else
                {
                    MessageBox.Show("Preencher a Senha");
                    txtCliSenha.Focus();
                }
            }
        }

        private void BtnFoto_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            dlg.InitialDirectory = "c:\\";
            dlg.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                arquivoSelecionado = dlg.FileName;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(arquivoSelecionado);
                bitmap.EndInit();
                imgFoto.Source = bitmap;


            }
        }

        private void BtnCliAdd_Click(object sender, RoutedEventArgs e)
        {
            string status;
            DateTime data = dataCliCad.DisplayDate;
            Banco bd = new Banco();
            bd.Conectar();
            if (ChkStatus.IsChecked == true)
            {
                status = "ATIVO";
                string inserir = "INSERT INTO cliente(nomeCli, emailCli,senhaCli,statusCli,dataCadCli,fotoCli)VALUES('" +
                    txtCliNome.Text + "','" +
                    txtCliEmail.Text + "','" +
                    txtCliSenha.Text + "','" +
                    status + "','" +
                    data.ToString("yyyy-MM-dd") + "','" +
                    imgFoto.Source + "')";
                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            else
            {
                status = "INATIVO";
                string inserir = "INSERT INTO cliente(nomeCli, emailCli,senhaCli,statusCli,dataCadCli,fotoCli)VALUES('" +
                    txtCliNome.Text + "','" +
                    txtCliEmail.Text + "','" +
                    txtCliSenha.Text + "','" +
                    status + "','" +
                    data.ToString("yyyy-MM-dd") + "','" +
                    imgFoto.Source + "')";
                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            bd.Desconectar();
            MessageBox.Show("Cliente cadastrado com sucesso!!!", "Cadastro de Cliente");
        }

        private void BtnCliFoneAlterar_Click(object sender, RoutedEventArgs e)
        {
            if ( dataCliTelefone.SelectedIndex >=0)
            {
                var rowView = dataCliTelefone.SelectedItems[0] as DataRowView;

                FoneCliente alterar = new FoneCliente();
                alterar.Show();
                alterar.codigoFone = rowView["idfoneCli"].ToString();
                alterar.txtFoneCliente.Text = rowView["numeroFone"].ToString();
                alterar.cmbTipo.Text = rowView["tipoFone"].ToString();
                alterar.cmbDescricao.Text = rowView["descFone"].ToString();
                alterar.codigoCli = rowView["idcliente"].ToString();

                //buscar o cliente no banco para mostrar somente o nome
                Banco bd = new Banco();
                bd.Conectar();

                string selecionar = "SELECT nomeCli FROM cliente WHERE idcliente = ?";
                MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
                com.Parameters.Clear();
                com.Parameters.Add("@idcliente", MySqlDbType.String).Value = alterar.codigoCli);
                com.CommandType = CommandType.Text; /* Executa o comando */
                                                    //Recebe conteúdo do banco
                MySqlDataReader dr = com.ExecuteReader();
                dr.Read();
                alterar.cmbNome.Text = dr.GetString(0);
                alterar.txtFoneCliente.IsEnabled = true;
                alterar.txtFoneCliente.Focus();
                alterar.btnFoneAdd.IsEnabled = false;
                alterar.btnFoneAlterar.IsEnabled = true;
            }
        }

        private void BtnCliFoneRemover_Click(object sender, RoutedEventArgs e)
        {
            if (dataCliTelefone.SelectedIndex >0)
            {
                var rowView = dataCliTelefone.SelectedItems[0] as DataRowView;

                string codigoFone = rowView["idfoneCli"].ToString();

                Banco bd = new Banco();
                bd.Conectar();
                string deletar = "DELETE FROM fonecli WHERE idfonecli='" + codigoFone "'";
                MySqlCommand comandos = new MySqlCommand(deletar, bd.conexao);
                comandos.ExecuteNonQuery();
                bd.Desconectar();
                MessageBox.Show("Telefone removido com sucesso!!!", "Remoção de telefones");

            }
            System.Text.RegularExpressions.Regex num = new System.Text.RegularExpressions.Regex("[^0-9]");
            
            if (!num.IsMatch(this.txtCliCodigo.Text))
            {
                Banco bd = new Banco();
                bd.Conectar();

                string selecionar = "SELECT * FROM fonecli WHERE idcliente = ?";
                MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
                com.Parameters.Clear();
                com.Parameters.Add("@idcliente", MySqlDbType.String).Value = txtCliCodigo.Text;
                com.CommandType = CommandType.Text;
                MySqlDataAdapter da = new MySqlDataAdapter(com);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataCliTelefone.DisplayMemberPath = "idcliente";
                dataCliTelefone.ItemsSource = dt.DefaultView;
            }
        }
    }
}
