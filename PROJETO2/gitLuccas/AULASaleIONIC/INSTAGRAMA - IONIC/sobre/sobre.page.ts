import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http, Headers, Response } from '@angular/http';
import { UrlService } from '../../servidor/url.service';

@Component({
  selector: 'app-sobre',
  templateUrl: './sobre.page.html',
  styleUrls: ['./sobre.page.scss'],
})
export class SobrePage implements OnInit {

  instas: any;

  constructor(public http: Http, public servidorUrl: UrlService) {
    this.listaInsta();
   }

   listaInsta(){
    this.http.get(this.servidorUrl.getInsta()).pipe(map(res => res.json()))
    .subscribe(
      listaDados => {
        this.instas = listaDados.graphql.user.edge_owner_to_timeline_media.edges;
        console.log(this.instas[0].node.thumbnail_resources[4].src);
      }
    );
 }

  ngOnInit() {
  }

}
