import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  idCliente = "";
  nomeCliente = "";
  emailCliente = "";
  statusCliente = "";
  fotoCliente = "";

  constructor() { }

  setIdCliente(valor){ // Modificador
    this.idCliente = valor;
  }
  getIdCliente(){ // Acessor
    return this.idCliente;
  }

  setNomeCliente(valor){ // Modificador
    this.nomeCliente = valor;
  }
  getNomeCliente(){ // Acessor
    return this.nomeCliente;
  }

  setEmailCliente(valor){ // Modificador
    this.emailCliente = valor;
  }
  getEmailCliente(){ // Acessor
    return this.emailCliente;
  }

  setStatusCliente(valor){ // Modificador
    this.statusCliente = valor;
  }
  getStatusCliente(){ // Acessor
    return this.statusCliente;
  }

  setFotoCliente(valor){ // Modificador
    this.fotoCliente = valor;
  }
  getFotoCliente(){ // Acessor
    return this.fotoCliente;
  }



}
