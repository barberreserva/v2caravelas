import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})


export class UrlService {

  url: string = "http://allepalmeira.com.br/ti03-kibeleza/admin/";
  // url: string = "http://localhost:8080/ti03Kibeleza/admin/";

  urlInsta: string = "https://www.instagram.com/sistemasmp/?__a=1";

  constructor(public alerta: AlertController) { }

  getUrl(){
    return this.url;
  }

  getInsta(){
    return this.urlInsta; 
  }

  async alertas(titulo, msg){
    const alert = await this.alerta.create({
      header: titulo,
      message: msg,
      buttons: ['OK']
    });
    await alert.present();
  }

}
