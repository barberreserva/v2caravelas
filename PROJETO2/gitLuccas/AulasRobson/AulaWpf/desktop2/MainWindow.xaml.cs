﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace desktop2
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void form1_initialized(object sender, EventArgs e)
        {
            /*intaxe do messagebox.box:
            Messagebox.show(texto da mensagem, título da mensagem, opções de botões,icones)
            a sequência \n insere quebra de linha no texto da mensagem*/
            MessageBox.Show("Seja bem vindo ao formulário WPF! \n Pressione 'OK' para continuar",
                "título da caixa", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            Label1.Content = TextBox1.Text;
            TextBox1.Text = "";
            /* Ovalor das caixas sempre serão inicialmente reconhecido como texto
              Para*/  
        }

        private void BtnMais_Click(object sender, RoutedEventArgs e)
        {
            label2.FontSize = label2.FontSize + 2;
            label2.Width = label2.Width + 10;
            label2.Height = label2.Height + 10;
        }

        private void BtnMenos_Click(object sender, RoutedEventArgs e)
        {
            label2.FontSize = label2.FontSize - 2;
            label2.Width = label2.Width - 10;
            label2.Height = label2.Height - 10;
        }

        private void BtnVermelho_Click(object sender, RoutedEventArgs e)
        {
            label2.Foreground = Brushes.Red;
        }

        private void BtnAzul_Click(object sender, RoutedEventArgs e)
        {
            label2.Foreground = Brushes.Blue;
        }

        private void RadioButton1_Checked(object sender, RoutedEventArgs e)
        {
            TextBox2.IsEnabled = false;
        }

        private void RadioButton2_Checked(object sender, RoutedEventArgs e)
        {
            TextBox2.IsEnabled = true;
        }

        private void RadioButton3_Checked(object sender, RoutedEventArgs e)
        {
            TextBox3.Text = "opçao 1";
            TextBox3.Background = Brushes.Red;
        }

        private void RadioButton4_Checked(object sender, RoutedEventArgs e)
        {
            TextBox3.Text = "opçao 2";
            TextBox3.Background = Brushes.Blue;
        }

        private void Button6_Click(object sender, RoutedEventArgs e)
        {
            if(CheckBox1.IsChecked == true)
            {
                Label3.Content = "Caixa Marcada.";
            }
            else
            {
                Label3.Content = "-";
            }
            //----------------------------------
            if (CheckBox2.IsChecked == true)
            {
                Label4.Content = "Caixa Marcada.";
            }
            else
            {
                Label4.Content = "-";
            }
        }

        private void TextBox4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ListBox1.Items.Add(TextBox4.Text);
                TextBox4.Clear();
            }
        }

        private void ListBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                ListBox1.Items.Remove(ListBox1.SelectedItem);
            }
        }

        private void ListBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListBox1.SelectedItem != null)
            {
                TextBox4.Text = ListBox1.SelectedItem.ToString();
            }
        }

        private void Button7_Click(object sender, RoutedEventArgs e)
        {
            //valores da listbox para list (variável vetor)
            List<double> minhaLista = new List<double>();
            foreach (var item in ListBox1.Items)
            {
                minhaLista.Add(Convert.ToDouble(item));
            }
            //valores do list (variável vetor) para combobox
            foreach(var item in minhaLista)
            {
                ComboBox1.Items.Add(item);
            }
            ListBox1.Items.Clear();
        }

        private void Button8_Click(object sender, RoutedEventArgs e)
        {
            //Pergunta através de caixa de mensagem
            var result = MessageBox.Show
                ("Se você clicar em 'sim', os valores das listas serão apagados." +
                "\n Deseja apagar os valores das listas?", "Título da caixa",
                MessageBoxButton.YesNo, MessageBoxImage.Question);
            if(result == MessageBoxResult.Yes)
            {
                TextBox4.Clear();
                ComboBox1.Items.Clear();
                ListBox1.Items.Clear();
            } 
            else
            {
                return; //interrompe sequência das instruções
            }
        }
    }
}
