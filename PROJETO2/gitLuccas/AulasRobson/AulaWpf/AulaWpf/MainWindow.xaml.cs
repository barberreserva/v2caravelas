﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AulaWpf
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnCalcular_Click(object sender, RoutedEventArgs e)
        {
            double altura = Convert.ToDouble(textAltura.Text);
            double peso = Convert.ToDouble(textPeso.Text);
            double imc = peso / Math.Pow(altura, 2); //potência
            string classificacao;
            if (imc < 18.5){ classificacao = "abaixo do peso";
            }
            else if (imc <= 24.9){ classificacao = "peso ideal";
            }
            else if (imc <= 29.9){ classificacao = "levemente acima do peso";
            }
            else if (imc <= 34.9){ classificacao = "Obesidade grau 1";
            }
            else if (imc <= 39.9){ classificacao = "Obesidade grau 2";
            }
            else { classificacao = "Obesidade grau 3";
            }
            MessageBox.Show("IMC: " + string.Format("{0:0.00}", imc) + "\n Classificação: " + classificacao, "Cálculo IMC");
            


        }
    }
}
