﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace statusAluno
{
    /// <summary>
    /// Lógica interna para cursos.xaml
    /// </summary>
    public partial class cursos : Window
    {
        public cursos()
        {
            InitializeComponent();
            Atualizar.IsEnabled = false;
            tbCod.IsEnabled = false;
            montaGride();
        }
        private void montaGride()
        {
            //Preencher List ---------------------------------------------------------------------
            Banco banco = new Banco();
            banco.Conectar();
            var dataAdapter = new MySqlDataAdapter("SELECT * FROM cyrsos ORDER By titulo", banco.conexao);
            DataTable dt = new DataTable();
            dataAdapter.Fill(dt);

            var minhaLista = new List<dynamic>();
            foreach (DataRow linha in dt.Rows)
            {
                string status;
                if (Convert.ToBoolean(linha["ativo"].ToString()) == true)
                    {
                    status = "sim";
                    } else {
                    status = "não";
                    }
                minhaLista.Add(new
                {
                    Id = linha["id"].ToString(),
                    Titulo = linha["titulo"].ToString(),
                    CH = linha["ch"].ToString(),
                    Ativo = status
                });
            }
            dgCursos.ItemsSource = minhaLista; //Preencher dataGrid com dados da List
            banco.Desconectar();
        }
    }
}
