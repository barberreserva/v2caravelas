﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace statusAluno
{
    class Banco
    {
        string db = "SERVER=localhost; USER=root; DATABASE=escola";
        public MySqlConnection conexao;

        public void Conectar()// Método de conexão
        {
            try //exceções
            {
                conexao = new MySqlConnection(db);
                conexao.Open();
            } catch {
                MessageBox.Show("Erro tentar estabelecer uma conexão com o banco de dados", "Erro!");
            }
        }
        public void Desconectar() // método para fechar a conexão 
        {
            try // Exceções
            {
                conexao = new MySqlConnection(db);
                conexao.Close();
            } catch {
                MessageBox.Show("Falha ao tentar se comunicar com o banco de dados", "ERRO!");
            }
        }
    }
}
