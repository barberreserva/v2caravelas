﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace statusAluno
{
    /// <summary>
    /// Lógica interna para Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();
            string inserir = "INSERT INTO cursos(titulo,ch,ativo)values('" +
                tbCurso.Text + "' ," + tbCh.Text + "," + true + ")";
            MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
            comandos.ExecuteNonQuery();
            bd.Desconectar();
            MessageBox.Show("Curso cadastrado com sucesso!", "Cadastro de Cursos");
            tbCurso.Clear();
            tbCh.Clear();
            tbCurso.Focus();
        }
    }
}
