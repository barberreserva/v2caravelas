import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs/pg',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pg/home/home.module').then(m => m.HomePageModule)
          }
        ]
      },
      {
        path: 'sobre',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pg/sobre/sobre.module').then(m => m.SobrePageModule)
          }
        ]
      },
      {
        path: 'servico',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pg/servico/servico.module').then(m => m.ServicoPageModule)
          }
        ]
      },
      {
        path: 'login',
        children:[
          {
            path:'',
            loadChildren:() =>
            import('../pg/login/login.module').then(m => m.LoginPageModule)
          
          }
        ]
      },
        { 
        redirectTo: '/tabs/pg/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/pg/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
