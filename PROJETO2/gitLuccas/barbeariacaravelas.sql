-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 08-Nov-2019 às 15:39
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `barbeariacaravelas`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `nomeCli` varchar(100) NOT NULL,
  `emailCli` varchar(100) NOT NULL,
  `senhaCli` varchar(45) NOT NULL,
  `statusCli` varchar(45) NOT NULL,
  `dataCadCli` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fotoCli` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`idcliente`, `nomeCli`, `emailCli`, `senhaCli`, `statusCli`, `dataCadCli`, `fotoCli`) VALUES
(1, 'Douglas Santos', 'doug@hotmail.com', '123456', 'ativo', '2019-10-29 14:07:43', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

CREATE TABLE `empresa` (
  `idempresa` int(11) NOT NULL,
  `nomeEmp` varchar(100) NOT NULL,
  `cnpjCpf` varchar(45) NOT NULL,
  `razaoSocial` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `statusEmp` varchar(45) NOT NULL,
  `dataCadEmp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `horarioAtend` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `empresa`
--

INSERT INTO `empresa` (`idempresa`, `nomeEmp`, `cnpjCpf`, `razaoSocial`, `email`, `statusEmp`, `dataCadEmp`, `horarioAtend`) VALUES
(1, 'Barbearia Caravelas', '23.456.789/1910-01', 'Barbearia Caravelas', 'barbercaravelas019@gmail.com', 'Ativa', '2019-09-17 03:00:00', '2019-09-26 11:20:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fonecli`
--

CREATE TABLE `fonecli` (
  `idfoneCli` int(11) NOT NULL,
  `numeroFone` varchar(45) NOT NULL,
  `tipoFone` varchar(45) NOT NULL,
  `descFone` varchar(45) NOT NULL,
  `idcliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `fonecli`
--

INSERT INTO `fonecli` (`idfoneCli`, `numeroFone`, `tipoFone`, `descFone`, `idcliente`) VALUES
(1, '987554217', 'celular', 'celular', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `foneemp`
--

CREATE TABLE `foneemp` (
  `idfoneEmp` int(11) NOT NULL,
  `numeroFone` varchar(45) NOT NULL,
  `tipoFone` varchar(45) NOT NULL,
  `descFone` varchar(45) NOT NULL,
  `idempresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `foneemp`
--

INSERT INTO `foneemp` (`idfoneEmp`, `numeroFone`, `tipoFone`, `descFone`, `idempresa`) VALUES
(1, '27982378', 'Empresa', 'Contato', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fonefunc`
--

CREATE TABLE `fonefunc` (
  `idfoneFunc` int(11) NOT NULL,
  `numeroFone` varchar(45) NOT NULL,
  `tipoFone` varchar(45) NOT NULL,
  `descFone` varchar(45) NOT NULL,
  `idfuncionario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `fonefunc`
--

INSERT INTO `fonefunc` (`idfoneFunc`, `numeroFone`, `tipoFone`, `descFone`, `idfuncionario`) VALUES
(1, '986645321', 'celular', 'Telefone celular', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `idfuncionario` int(11) NOT NULL,
  `nomeFunc` varchar(100) NOT NULL,
  `emailFunc` varchar(100) NOT NULL,
  `senhaFunc` varchar(45) NOT NULL,
  `nivelFunc` varchar(45) NOT NULL,
  `statusFunc` varchar(45) NOT NULL,
  `dataCadFunc` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `horarioTrabalho` time NOT NULL,
  `idempresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `funcionario`
--

INSERT INTO `funcionario` (`idfuncionario`, `nomeFunc`, `emailFunc`, `senhaFunc`, `nivelFunc`, `statusFunc`, `dataCadFunc`, `horarioTrabalho`, `idempresa`) VALUES
(1, 'Kamau', 'kamau@hotmail.com', '123456', 'Cabeleireiro', 'ativo', '2019-09-11 03:00:00', '08:30:00', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `reserva`
--

CREATE TABLE `reserva` (
  `idcliente` int(11) NOT NULL,
  `idservico` int(11) NOT NULL,
  `dataReserva` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `statusReserva` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `reserva`
--

INSERT INTO `reserva` (`idcliente`, `idservico`, `dataReserva`, `statusReserva`) VALUES
(1, 1, '2019-09-10 17:40:00', 'Ativo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servico`
--

CREATE TABLE `servico` (
  `idservico` int(11) NOT NULL,
  `nomeServico` varchar(100) NOT NULL,
  `valorServico` decimal(10,0) NOT NULL,
  `statusServico` varchar(45) NOT NULL,
  `dataCadServico` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fotoServico1` varchar(250) NOT NULL,
  `fotoServico2` varchar(250) NOT NULL,
  `fotoServico3` varchar(250) NOT NULL,
  `descServico` text NOT NULL,
  `tempoServico` time NOT NULL,
  `idempresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `servico`
--

INSERT INTO `servico` (`idservico`, `nomeServico`, `valorServico`, `statusServico`, `dataCadServico`, `fotoServico1`, `fotoServico2`, `fotoServico3`, `descServico`, `tempoServico`, `idempresa`) VALUES
(1, 'Corte', '25', 'Ativo', '2019-09-13 03:00:00', 'C:\\Users\\luccas.bsantiago\\Documents\\BarberCaravelas_DB\\tjpg', 'C:\\Users\\luccas.bsantiago\\Documents\\BarberCaravelas_DB\\tjpg', 'C:\\Users\\luccas.bsantiago\\Documents\\BarberCaravelas_DB\\tjpg', 'Corte de cabelo', '35:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `fonecli`
--
ALTER TABLE `fonecli`
  ADD PRIMARY KEY (`idfoneCli`),
  ADD KEY `idcliente` (`idcliente`);

--
-- Indexes for table `foneemp`
--
ALTER TABLE `foneemp`
  ADD PRIMARY KEY (`idfoneEmp`),
  ADD KEY `idempresa` (`idempresa`);

--
-- Indexes for table `fonefunc`
--
ALTER TABLE `fonefunc`
  ADD PRIMARY KEY (`idfoneFunc`),
  ADD KEY `idfuncionario` (`idfuncionario`);

--
-- Indexes for table `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`idfuncionario`),
  ADD KEY `idempresa` (`idempresa`);

--
-- Indexes for table `servico`
--
ALTER TABLE `servico`
  ADD PRIMARY KEY (`idservico`),
  ADD KEY `idempresa` (`idempresa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `empresa`
--
ALTER TABLE `empresa`
  MODIFY `idempresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fonecli`
--
ALTER TABLE `fonecli`
  MODIFY `idfoneCli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `foneemp`
--
ALTER TABLE `foneemp`
  MODIFY `idfoneEmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fonefunc`
--
ALTER TABLE `fonefunc`
  MODIFY `idfoneFunc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `idfuncionario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `servico`
--
ALTER TABLE `servico`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `fonecli`
--
ALTER TABLE `fonecli`
  ADD CONSTRAINT `fonecli_ibfk_1` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `foneemp`
--
ALTER TABLE `foneemp`
  ADD CONSTRAINT `foneemp_ibfk_1` FOREIGN KEY (`idempresa`) REFERENCES `empresa` (`idempresa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `fonefunc`
--
ALTER TABLE `fonefunc`
  ADD CONSTRAINT `fonefunc_ibfk_1` FOREIGN KEY (`idfuncionario`) REFERENCES `funcionario` (`idfuncionario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `funcionario`
--
ALTER TABLE `funcionario`
  ADD CONSTRAINT `funcionario_ibfk_1` FOREIGN KEY (`idempresa`) REFERENCES `kibeleza_novo`.`empresa` (`idempresa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `servico`
--
ALTER TABLE `servico`
  ADD CONSTRAINT `servico_ibfk_1` FOREIGN KEY (`idempresa`) REFERENCES `empresa` (`idempresa`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
