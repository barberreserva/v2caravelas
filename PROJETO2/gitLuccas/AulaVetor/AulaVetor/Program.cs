﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AulaVetor
{
    class Program
    {
        static void Main(string[] args)
        {
            //Despesas semestrais do cartão de crédito
            double[] cartao = new double[6];
            Console.WriteLine
                ("Digite o valor das faturas do 1º semestre");
            cartao[0] = double.Parse(Console.ReadLine());
            cartao[1] = double.Parse(Console.ReadLine());
            cartao[2] = double.Parse(Console.ReadLine());
            cartao[3] = double.Parse(Console.ReadLine());
            cartao[4] = double.Parse(Console.ReadLine());
            cartao[5] = double.Parse(Console.ReadLine());

            Console.Write("Despesas com cartão no 1º semestre: " + string.Format("{0:C}", cartao.Sum()));
            //exibindo no formato moéda ("{0:C}" - currency)

            Console.ReadKey();
        }
    }
}
