﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atividade02092019
{
    class Program
    {
        static void Main(string[] args)
        {
            //Cálculo de tabuada
            Console.WriteLine
                ("Digite um número para calcular sua tabuada: ");
            int tabuada = int.Parse(Console.ReadLine());
            int i = 0;
            while (i <= 10)
            {
                Console.WriteLine(tabuada + " X " + i + " = " + (tabuada * i));
                i = i + 1; // variável de controle
            }
            Console.ReadKey(); 
        }
    }
}
