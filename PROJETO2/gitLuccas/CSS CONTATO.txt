.sobre2 p{
	margin-top: 10%;
	font-size: 15px;
    padding: 5% 5% 1% 2%;
    line-height: 1;
    color: #ffff;
}

.sobre2 h2{
	margin-top: 15%;
	font-size: 45px;
    padding: 2% 2% 2% 2%;
    line-height: 1;
    color: #FFFFFF;
}

.form{padding: 2%;}

.form input[type=text],
.form input[type=tel],
.form input[type=email],
.form textarea{
	background: #f8f6fa;
    border: solid thin #333;
    width: 50%;
    padding: 1%;
    color: #333;
	margin-bottom: 2%;
    border-radius: 5px;
}
.form textarea{resize: none;}

.form input[type=submit]{
	float: right;
	margin-right: -13px;
    border-radius: 5px;
	background: #262626;
	color: #F8F8F8;
	border: solid .5px #909090;
	padding: 2% 8%;
	cursor: pointer;
	font-size: 0.8em;
}
.form input[type=submit]:hover{
	background: #F8F8F8;
	color: #262626;
	border: solid .5px #262626;