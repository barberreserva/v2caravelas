﻿using System; //recursos utilizados
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atividade1 //escopo do objeto atividade
{
    class Program //classe do programa
    {
        static void Main(string[] args) //método main
        {
            // --- Nome e Idade ---
            string nome, sobrenome; //declaração de variáveis
            int idade;

            Console.Write("Digite seu nome: "); // saída
            nome = Console.ReadLine(); //entrada

            Console.Write("Digite seu sobrenome: "); //saída
            sobrenome = Console.ReadLine(); //entrada

            Console.WriteLine("Digite sua idade: "); //saída
            idade = int.Parse(Console.ReadLine()); //entrada

            Console.WriteLine();
            Console.WriteLine("---------------------------");
            Console.WriteLine();

            // concatenando saída
            Console.Write(nome + " " + sobrenome + " tem " + idade + " anos de idade. ");
            Console.ReadKey(); // Interrompe execução
        }
    }
}
