﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atividade2
{
    class Program
    {
        static void Main(string[] args)
        {
            // ---Somando dois números inteiros ---

            int n1, n2, resultado; //declaração de variáveis

            Console.Write("Digite primeiro número: "); //Saída

            n1 = int.Parse(Console.ReadLine()); //entrada

            Console.Write("digite o segundo número: ");

            n2 = int.Parse(Console.ReadLine());

            resultado = n1 + n2; //processamento

            Console.WriteLine("O Resultado da soma é " + resultado + ".");

            Console.ReadKey(); //interrompe execução

        }
    }
}
