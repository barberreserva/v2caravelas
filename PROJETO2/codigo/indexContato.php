<!DOCTYPE html>
<html lang="pt-br">
    <head>
   <meta charset="utf-8">
   <title> Barbearia Caravelas</title>
   
   <!--CSS de reset das configurações do browser-->
   <link rel="stylesheet" type="text/css" href="css/reset.css">
   <link rel="icon" type="image/png" sizes="32x32" href ="img/icon/lg.png">
   <link rel="stylesheet" type="text/css" href="css/slick.css">
   <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
   <link rel="stylesheet" type="text/css" href="css/lity.css">
   <link rel="stylesheet" type="text/css" href="css/animate.css">
   <link rel="stylesheet" type="text/css" href="css/animate1.css">
   <link rel="stylesheet" type="text/css" href="css/contato.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
   <script type="text/javascript">
   jQuery(document).ready(function(){
   
   jQuery("#subirTopo").hide();
   
   jQuery('a#subirTopo').click(function () {
            jQuery('body,html').animate({
              scrollTop: 0
            }, 800);
           return false;
      });
   
   jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 1000) {
               jQuery('#subirTopo').fadeIn();
            } else {
               jQuery('#subirTopo').fadeOut();
            }
        });
   });
   </script>
   </head>
   

</div>
    <body>  
            <!--CABEÇALHO DO SITE-->
        <header id="topo-fixo">     
                <!-- <div class="logo">
                        <img src="img/fff.png" alt="logo" width="800" height="800" />
                    </div>                   -->
                <div class="site topo"> 
                        <h1> Barbearia Caravelas</h1>      
                     <nav>      
                        <ul><!--Lista de marcadores-->
                            <li>
                                <a href="index.php">
                                    <span class="icon iconInicio"></span>
                                        <span>INICIO</span>        
                                </a>
                            </li><!--Fim item da lista-->
                            <li>
                                <a href="indexServico.php">
                                    <span class="icon iconServico"></span>
                                    <span>UNIDADES</span>
                                </a>
                            </li>
                            <div class="logo">
                                    <img src="img/fff.png" alt="logo" width="120" height="120"  />
                                </div>  
                            <li>
                                <a href="#barbeiro">
                                    <span class="icon iconGaleria"></span>
                                    <span>BARBEIROS</span>
                                </a> 
                                

                            </li>
                            <li>
                                <a href="contato.php">
                                    <span class="icon iconContato"></span>
                                    <span>CONTATO</span>
                                </a>

                            </li>

                       </ul>            
                    </nav>
                </div>                   
        </header>     
<div class="duvida">
<img src="img/texto1.png">
</div>







<div class="mapa">

        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.137154360111!2d-46.65487588440678!3d-23.5635170675584!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59c872682a63%3A0x7c0315ee4514f73b!2sAv.+Paulista%2C+1230+-+Bela+Vista%2C+S%C3%A3o+Paulo+-+SP%2C+01310-000!5e0!3m2!1spt-BR!2sbr!4v1565187665852!5m2!1spt-BR!2sbr" width="1900" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
        <footer class="rodape">
                <p>Karla e Luccas - Todos os direitos reservados</p>
                <a id="subirTopo">
                        <img src="img/subir.png" width="5" height="5">
                       </a>
            </footer>
        </body>
        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/slick.js"></script>
        <script type="text/javascript" src="js/wow.js"></script>
        <script type="text/javascript" src="js/lity.js"></script>
        <script type="text/javascript" src="js/animacao.js"></script>
        <script src="https://cdn.shopify.com/s/files/1/0683/5883/t/3/assets/instafeed.min.js"></script>

        <script src="https://cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>

        <script src="js/instafeed.js"></script>
        </html>
        