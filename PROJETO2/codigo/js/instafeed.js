var feed = new Instafeed({

    get: 'user',
    userId: 15367158079,
    accessToken: '15367158079.aeeff59.8e5d4f698a0d497d802307a9e14be06a',
    template: '<div><img src="{{image}}" title="{{caption}}"/></div>',
    target: 'instafeed',
    after: function () {
        $('#instafeed').slick({
            dots: false,
            infinite: true,
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            responsive: [{
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 6
                    }
                }, {
                    breakpoint: 601,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }]
        });
    }
});

feed.run();