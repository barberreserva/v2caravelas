﻿#pragma checksum "..\..\funcCadastro.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "D50781008E9F8B5F2C41730494E89CF88A4B46B5"
//------------------------------------------------------------------------------
// <auto-generated>
//     O código foi gerado por uma ferramenta.
//     Versão de Tempo de Execução:4.0.30319.42000
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------

using SistemaCaravelas;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SistemaCaravelas {
    
    
    /// <summary>
    /// funcCadastro
    /// </summary>
    public partial class funcCadastro : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 1 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SistemaCaravelas.funcCadastro funcCadastro1;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFuncCodigo;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFuncNome;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFuncEmail;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFuncSenha;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbFuncNivel;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem ADMIN;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem NIVEL1;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem NIVEL2;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem NIVEL3;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem NIVEL4;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle rectangleStatus;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblStatus;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chkStatus;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblNivel;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dataFuncCad;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbFuncCargaH;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem H4;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem H430;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem H5;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem H530;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem H6;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem H630;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem H7;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem H730;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem H8;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblcargahoraria;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbFuncEmpresa;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblEmpresa;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTelefone;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dataFuncTelefone;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFuncFoneAdd;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFuncFoneAlterar;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFuncFoneRemover;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFuncAdd;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFuncAlterar;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\funcCadastro.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFecharCadFunc;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SistemaCaravelas;component/funccadastro.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\funcCadastro.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.funcCadastro1 = ((SistemaCaravelas.funcCadastro)(target));
            
            #line 8 "..\..\funcCadastro.xaml"
            this.funcCadastro1.Loaded += new System.Windows.RoutedEventHandler(this.FuncCadastro1_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.txtFuncCodigo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.txtFuncNome = ((System.Windows.Controls.TextBox)(target));
            
            #line 15 "..\..\funcCadastro.xaml"
            this.txtFuncNome.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxtFuncNome_KeyDown);
            
            #line default
            #line hidden
            return;
            case 4:
            this.txtFuncEmail = ((System.Windows.Controls.TextBox)(target));
            
            #line 16 "..\..\funcCadastro.xaml"
            this.txtFuncEmail.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxtFuncEmail_KeyDown);
            
            #line default
            #line hidden
            return;
            case 5:
            this.txtFuncSenha = ((System.Windows.Controls.TextBox)(target));
            
            #line 17 "..\..\funcCadastro.xaml"
            this.txtFuncSenha.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxtFuncSenha_KeyDown);
            
            #line default
            #line hidden
            return;
            case 6:
            this.cmbFuncNivel = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.ADMIN = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 8:
            this.NIVEL1 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 9:
            this.NIVEL2 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 10:
            this.NIVEL3 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 11:
            this.NIVEL4 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 12:
            this.rectangleStatus = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 13:
            this.lblStatus = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.chkStatus = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 15:
            this.lblNivel = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.dataFuncCad = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 17:
            this.cmbFuncCargaH = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 18:
            this.H4 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 19:
            this.H430 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 20:
            this.H5 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 21:
            this.H530 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 22:
            this.H6 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 23:
            this.H630 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 24:
            this.H7 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 25:
            this.H730 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 26:
            this.H8 = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 27:
            this.lblcargahoraria = ((System.Windows.Controls.Label)(target));
            return;
            case 28:
            this.cmbFuncEmpresa = ((System.Windows.Controls.ComboBox)(target));
            
            #line 42 "..\..\funcCadastro.xaml"
            this.cmbFuncEmpresa.IsMouseCapturedChanged += new System.Windows.DependencyPropertyChangedEventHandler(this.CmbFuncEmpresa_IsMouseCapturedChanged);
            
            #line default
            #line hidden
            return;
            case 29:
            this.lblEmpresa = ((System.Windows.Controls.Label)(target));
            return;
            case 30:
            this.lblTelefone = ((System.Windows.Controls.Label)(target));
            return;
            case 31:
            this.dataFuncTelefone = ((System.Windows.Controls.DataGrid)(target));
            
            #line 46 "..\..\funcCadastro.xaml"
            this.dataFuncTelefone.Loaded += new System.Windows.RoutedEventHandler(this.dataFuncTelefone_Loaded);
            
            #line default
            #line hidden
            return;
            case 32:
            this.btnFuncFoneAdd = ((System.Windows.Controls.Button)(target));
            
            #line 47 "..\..\funcCadastro.xaml"
            this.btnFuncFoneAdd.Click += new System.Windows.RoutedEventHandler(this.BtnFuncFoneAdd_Click);
            
            #line default
            #line hidden
            return;
            case 33:
            this.btnFuncFoneAlterar = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\funcCadastro.xaml"
            this.btnFuncFoneAlterar.Click += new System.Windows.RoutedEventHandler(this.BtnFuncFoneAlterar_Click);
            
            #line default
            #line hidden
            return;
            case 34:
            this.btnFuncFoneRemover = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\funcCadastro.xaml"
            this.btnFuncFoneRemover.Click += new System.Windows.RoutedEventHandler(this.btnFuncFoneRemover_Click);
            
            #line default
            #line hidden
            return;
            case 35:
            this.btnFuncAdd = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\funcCadastro.xaml"
            this.btnFuncAdd.Click += new System.Windows.RoutedEventHandler(this.BtnFuncAdd_Click);
            
            #line default
            #line hidden
            return;
            case 36:
            this.btnFuncAlterar = ((System.Windows.Controls.Button)(target));
            
            #line 51 "..\..\funcCadastro.xaml"
            this.btnFuncAlterar.Click += new System.Windows.RoutedEventHandler(this.btnFuncAlterar_Click);
            
            #line default
            #line hidden
            return;
            case 37:
            this.btnFecharCadFunc = ((System.Windows.Controls.Button)(target));
            
            #line 53 "..\..\funcCadastro.xaml"
            this.btnFecharCadFunc.Click += new System.Windows.RoutedEventHandler(this.BtnFecharCadFunc_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

