﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaCaravelas
{
    /// <summary>
    /// Lógica interna para servicoCadastro.xaml
    /// </summary>
    public partial class servicoCadastro : Window
    {
        
        public string codigoEmpresa, empresa;
        public double valor;
        string arquivo1, arquivo2, arquivo3;

        

        public servicoCadastro()
        {
            InitializeComponent();
            txtServNome.Focus();
            txtServNome.SelectAll();

            System.Text.RegularExpressions.Regex num = new System.Text.RegularExpressions.Regex("[^0-9]");
            //se for número
            if (!num.IsMatch(this.txtServCodigo.Text))
            {
                txtServValor.IsEnabled = true;
                chkStatus.IsEnabled = true;
                dataServCad.IsEnabled = true;
                btnFoto1.IsEnabled = true;
                btnFoto2.IsEnabled = true;
                btnFoto3.IsEnabled = true;
                txtServDescricao.IsEnabled = true;
                txtServDuracao.IsEnabled = true;
                cmbServEmpresa.IsEnabled = true;

                btnServAlterar.IsEnabled = true;
                btnServAdicionar.IsEnabled = false;
            }
        }

       

        private void TxtServNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtServNome.Text != "")
                {
                    txtServValor.IsEnabled = true;
                    txtServValor.Focus();
                    txtServValor.SelectAll();
                }
                else
                {
                    MessageBox.Show("Preencher o nome do serviço");
                    txtServNome.Focus();
                }
            }
        }

        private void TxtServValor_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) || (e.Key == Key.Tab))
                //não aceita textbox vazia
                if (string.IsNullOrEmpty(((TextBox)sender).Text))
                {
                    txtServValor.Text = "";
                }
                else
                {
                    //trabalha os valores somente número e vírgula
                    double num = 0;
                    bool success = double.TryParse(((TextBox)sender).Text, out num);
                    if (success & num >= 0)
                    {
                        ((TextBox)sender).Text.Trim();
                        txtServValor.Text = ((TextBox)sender).Text;

                        valor = double.Parse(txtServValor.Text);
                        txtServValor.Text = "R$ " + valor.ToString("N2");

                        chkStatus.IsEnabled = true;
                        MessageBox.Show("Não esqueça do **STATUS** do Serviço");
                        dataServCad.IsEnabled = true;
                        dataServCad.Focus();
                        txtServDescricao.IsEnabled = true;
                        btnFoto1.IsEnabled = true;
                    }
                    else
                    {
                        //não aceita texto
                        ((TextBox)sender).Text = txtServValor.Text;
                        ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                        txtServValor.Text = "";
                    }
                }
        }

        private void TxtServDescricao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtServDescricao.Text != "")
                {
                    txtServDuracao.IsEnabled = true;
                    txtServDuracao.Focus();
                    txtServDuracao.SelectAll();
                }
                else
                {
                    MessageBox.Show("Preencher a descrição do serviço");
                    txtServDescricao.Focus();
                }
            }
        }

        private void TxtServDuracao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtServDuracao.Text != "")
                {
                    cmbServEmpresa.IsEnabled = true;
                    cmbServEmpresa.Focus();
                }
                else
                {
                    MessageBox.Show("Preencher a duração do serviço");
                    txtServDescricao.Focus();
                }
            }
        }

        private void CmbServEmpresa_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (cmbServEmpresa.Text !="")
            {
                Banco bd = new Banco();
                bd.Conectar();

                string selecionar = "SELECT idempresa, nomeEmp FROM empresa WHERE nomeEmp = ?";
                MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
                com.Parameters.Clear();
                com.Parameters.Add("@nomeEmp", MySqlDbType.String).Value = cmbServEmpresa.Text;
                com.CommandType = CommandType.Text; /*Executa o comando*/
                                                    //Recebo conteúdo do Banco
                MySqlDataReader dr = com.ExecuteReader();
                dr.Read();
                codigoEmpresa = dr.GetString(0);

                System.Text.RegularExpressions.Regex num = new System.Text.RegularExpressions.Regex("[^0-9]");
                //se não for numero
                if(num.IsMatch(txtServCodigo.Text))
                {
                    btnServAdicionar.IsEnabled = true;
                }
            }
        }

        private void ServicoCadastro_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT * FROM empresa ORDER BY nomeEmp";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cmbServEmpresa.DisplayMemberPath = "nomeEmp";
            cmbServEmpresa.ItemsSource = dt.DefaultView;
        }

        private void BtnServAdicionar_Click(object sender, RoutedEventArgs e)
        {
            string status;
            DateTime data = dataServCad.DisplayDate;

            Banco bd = new Banco();
            bd.Conectar();
            if (chkStatus.IsChecked == true)
            {
                status = "ATIVO";
                string inserir = "INSERT INTO servico(nomeServico,valorServico,statusServico,dataCadServico,fotoServico1,fotoServico2,fotoServico3,descServico,tempoServico,idempresa)VALUES('" + txtServNome.Text + "','" + valor + "','" + status + "','" + data.ToString("yyyy-MM-dd") + "','" + txtServFoto1.Text + "','" + txtServFoto2.Text + "','" + txtServFoto3.Text + "','" + txtServDescricao.Text + "','" + txtServDuracao.Text + "','" + codigoEmpresa + "')";
                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            else
            {
                status = "INATIVO";
                string inserir = "INSERT INTO servico(nomeServico,valorServico,statusServico,dataCadServico,fotoServico1,fotoServico2,fotoServico3,descServico,tempoServico,idempresa)VALUES('" + txtServNome.Text + "','" + valor + "','" + status + "','" + data.ToString("yyyy-MM-dd") + "','" + txtServFoto1.Text + "','" + txtServFoto2.Text + "','" + txtServFoto3.Text + "','" + txtServDescricao.Text + "','" + txtServDuracao.Text + "','" + codigoEmpresa + "')";
                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            bd.Desconectar();
            MessageBox.Show("Serviço cadastrado com sucesso!", "Cadastro de Serviços");
        }

        private void BtnServAlterar_Click(object sender, RoutedEventArgs e)
        {
            string status;
            DateTime data = dataServCad.DisplayDate;

            Banco bd = new Banco();
            bd.Conectar();
            if (chkStatus.IsChecked == true)
            {
                status = "Ativo";
                string alterar = "UPDATE servico SET " + "nomeServico='" + txtServNome.Text + "'," + "valorServico='" + valor + "'," + "statusServico='" + status + "'," + "dataCadServico='" + data.ToString("yyyy-MM-dd") + "'," + "fotoServico1='" + txtServFoto1.Text + "'," + "fotoServico2='" + txtServFoto2.Text + "'," + "fotoServico3='" + txtServFoto3.Text + "'," + "descServico='" + txtServDescricao.Text + "'," + "tempoServico='" + txtServDuracao.Text + "'," + "idempresa='" + codigoEmpresa + "'" + "WHERE idservico='" + txtServCodigo.Text + "'";
                MySqlCommand comandos = new MySqlCommand(alterar, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            else
            {
                status = "INATIVO";
                string alterar = "UPDATE servico SET " + "nomeServico='" + txtServNome.Text + "'," + "valorServico='" + valor + "'," + "statusServico='" + status + "'," + "dataCadServico='" + data.ToString("yyyy-MM-dd") + "'," + "fotoServico1='" + txtServFoto1.Text + "'," + "fotoServico2='" + txtServFoto2.Text + "'," + "fotoServico3='" + txtServFoto3.Text + "'," + "descServico='" + txtServDescricao.Text + "'," + "tempoServico='" + txtServDuracao.Text + "'," + "idempresa='" + codigoEmpresa + "'" + "WHERE idservico='" + txtServCodigo.Text + "'";
                MySqlCommand comandos = new MySqlCommand(alterar, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            bd.Desconectar();
            MessageBox.Show("Serviço alterado com êxito", "Alteração de Serviços");
        }

        private void BtnFecharCadServ_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            Servico janelaServico = new Servico();
            janelaServico.Show();
        }

        private void BtnFoto1_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            dlg.InitialDirectory = "c:\\";
            dlg.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                arquivo1 = dlg.FileName;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(arquivo1);
                bitmap.EndInit();

                int foto1 = arquivo1.IndexOf("servico");
                string nomefoto1 = arquivo1.Substring(foto1 + 8);
                txtServFoto1.Text = nomefoto1;

                btnFoto2.IsEnabled = true;
            }
        }
        private void BtnFoto2_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            dlg.InitialDirectory = "c:\\";
            dlg.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                arquivo2 = dlg.FileName;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(arquivo2);
                bitmap.EndInit();

                int foto2 = arquivo2.IndexOf("servico");
                string nomefoto2 = arquivo2.Substring(foto2 + 8);
                txtServFoto2.Text = nomefoto2;

                btnFoto3.IsEnabled = true;
            }
        }
        private void BtnFoto3_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            dlg.InitialDirectory = "c:\\";
            dlg.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                arquivo3 = dlg.FileName;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(arquivo3);
                bitmap.EndInit();

                int foto3 = arquivo3.IndexOf("servico");
                string nomefoto3 = arquivo3.Substring(foto3 + 8);
                txtServFoto2.Text = nomefoto3;
            }
        }
    }
}
