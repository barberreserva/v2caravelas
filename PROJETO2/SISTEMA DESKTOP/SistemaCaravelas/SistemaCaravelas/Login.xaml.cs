﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SistemaCaravelas
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class login1 : Window
    {
        public string usuario, senha;
        public login1()
        {
            InitializeComponent();
            txtUsuario.Focus();
            txtUsuario.SelectAll();
        }

        private void TxtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtUsuario.Text == "")
                {
                    MessageBox.Show("Preencher o campo usuário corretamente!!!", "Login do Usuário");
                }
                else
                {
                    usuario = txtUsuario.Text;
                    txtSenha.IsEnabled = true;
                    txtSenha.Focus();

                }
            }
        }



        private void TxtSenha_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtSenha.Password == "")
                {
                    MessageBox.Show("Preencher o campo usuário corretamente!!!", "Senha do Usuário");
                }
                else
                {
                    senha = txtSenha.Password;
                    btnEntrar.IsEnabled = true;
                    btnEntrar.Focus();
                }
            }
        }

        private void btnFechar_Click(object sender, RoutedEventArgs e)
        {
            var sair = MessageBox.Show(
                          "Tem certeza que deseja ENCERRAR o sistema?",
                          "Encerrar Sistema", MessageBoxButton.YesNo,
                          MessageBoxImage.Exclamation);
            if (sair != MessageBoxResult.Yes)
            {
                return;
            }
            else
            {
                //Encerrar a aplicação corrente
                Application.Current.Shutdown();
            }
        }

        private void BtnEntrar_Click(object sender, RoutedEventArgs e)
        {
            if (usuario == "ADMIN" && senha == "1234")
            {
                Hide(); //Esconder
                menu1 frm = new menu1();
                frm.Show();
            }
            else if (usuario == "TESTE" && senha == "1234")
            {
                Hide();
                menu1 frm = new menu1();
                frm.Show();
                frm.btnCliente.IsEnabled = false;
                frm.btnFunc.IsEnabled = false;
            }
            else
            {
                MessageBox.Show("Favor preencher o usuário ou a senha corretamente!!!");
                txtUsuario.Clear();
                txtSenha.Clear();
                txtUsuario.Focus();
                txtSenha.IsEnabled = false;
                btnEntrar.IsEnabled = false;
            }
        }
    }
}

