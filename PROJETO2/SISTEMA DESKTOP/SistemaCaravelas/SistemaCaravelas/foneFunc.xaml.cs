﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaCaravelas
{
    /// <summary>
    /// Lógica interna para foneFunc.xaml
    /// </summary>
    public partial class foneFunc : Window
    {
        public string codigoCli;
        public string codigoFone;
        public foneFunc()
        {
            InitializeComponent();
            cmbNome.Focus();
        }
        private void Limpar()
        {
            txtFoneCli.Clear();
            cmbTipo.Text = "";
            cmbDescricao.Text = "";
            txtFoneCli.IsEnabled = false;
            cmbTipo.IsEnabled = false;
            cmbDescricao.IsEnabled = false;
            btnFoneAdd.IsEnabled = false;
        }

        private void BtnFecharFoneCli_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            funcCadastro janelaFuncCadastro = new funcCadastro();
            janelaFuncCadastro.Show();
        }

        private void BtnFoneAdd_Click(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string inserir = "INSERT INTO fonefunc(numeroFone,tipoFone,descFone,idfuncionario)VALUES('" + txtFoneCli.Text + "','" + cmbTipo.Text + "','" + cmbDescricao.Text + "','" + codigoCli + "')";
            MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
            comandos.ExecuteNonQuery();

            bd.Desconectar();
            MessageBox.Show("Telefone cadastrado com sucesso!!!", "Cadastro de Telefones");
            Limpar();
            txtFoneCli.IsEnabled = true;
            txtFoneCli.Focus();
        }

        private void foneFunc1_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT idfuncionario, nomeFunc FROM funcionario";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cmbNome.DisplayMemberPath = "nomeFunc";
            cmbNome.ItemsSource = dt.DefaultView;
        }

        private void BtnFoneAlterar_Click(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();
            string alterar = "UPDATE fonefunc SET " + "numeroFone='" + txtFoneCli.Text + "'," + "tipoFone='" + cmbTipo.Text + "'," + "descFone='" + cmbDescricao.Text + "'" + "WHERE idfoneFunc='" + codigoFone + "'";
            MySqlCommand comandos = new MySqlCommand(alterar, bd.conexao);
            comandos.ExecuteNonQuery();
            bd.Desconectar();
            MessageBox.Show("Telefone alterado com sucesso!!!", "Alteração de telefones");

            Funcionario frm = new Funcionario();
            frm.Show();
            Close();
        }

        private void CmbNome_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (cmbNome.Text != "")
            {
                Banco bd = new Banco();
                bd.Conectar();
                string selecionar = "SELECT * FROM funcionario WHERE nomeFunc = ?";
                MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
                com.Parameters.Clear();
                com.Parameters.Add("@nomeFunc", MySqlDbType.String).Value = cmbNome.Text;
                com.CommandType = CommandType.Text;
                MySqlDataReader dr = com.ExecuteReader();
                dr.Read();
                codigoCli = dr.GetString(0);
                txtFoneCli.IsEnabled = true;
                txtFoneCli.Focus();
            }
        }

        private void TxtFoneCli_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                cmbTipo.IsEnabled = true;
                cmbTipo.Focus();
            }
        }

        private void CmbDescricao_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnFoneAdd.IsEnabled = true;
            btnFoneAdd.Focus();
        }

        private void CmbTipo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbDescricao.IsEnabled = true;
            cmbDescricao.Focus();
        }
    }
}
