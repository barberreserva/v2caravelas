﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SistemaCaravelas
{
    public class Banco
    {
        /*string caminho = "SERVER=localhost;USER=root;DATABASE=barbeariacaravelas";*/
        string caminho = "SERVER=caravelasti03.mysql.dbaas.com.br;USERID=caravelasti03;DATABASE=caravelasti03;PWD=caravelasTi03;includesecurityasserts=true";
        public MySqlConnection conexao; 

        public void Conectar()
        {
            try
            {
                conexao = new MySqlConnection(caminho);
                conexao.Open();
            }
            catch
            {
                MessageBox.Show("Erro ao tentar estabelecer conexão com o Banco de Dados", "ERRO!"); 
            }
        }

        public void Desconectar()
        {
            try
            {
                conexao = new MySqlConnection(caminho);
                conexao.Close();
            }
            catch
            {
                MessageBox.Show("Erro ao tentar se comunicar com o Banco de Dados", "ERRO!"); 
            }
        }
    }
}
