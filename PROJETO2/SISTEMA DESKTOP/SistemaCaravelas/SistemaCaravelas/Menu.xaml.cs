﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaCaravelas
{
    /// <summary>
    /// Lógica interna para Window1.xaml
    /// </summary>
    public partial class menu1 : Window
    {
        public menu1()
        {
            InitializeComponent();
        }

        private void BtnFechar_Click(object sender, RoutedEventArgs e)
        {
            var sair = MessageBox.Show(
               "Tem certeza que deseja ENCERRAR o sistema?",
               "Encerrar Sistema", MessageBoxButton.YesNo,
               MessageBoxImage.Exclamation);
            if (sair != MessageBoxResult.Yes)
            {
                return;
            }
            else
            {
                //Encerrar a aplicação corrente
                Application.Current.Shutdown();
            }
        }

        private void BtnCliente_Click(object sender, RoutedEventArgs e)
        {
            Hide(); //Esconder atual
            Client janelaClient = new Client();
            janelaClient.Show();
        }

        private void BtnFunc_Click(object sender, RoutedEventArgs e)
        {
            Hide(); //Esconder atual
            Funcionario janelaFuncionario = new Funcionario();
            janelaFuncionario.Show();
        }

        private void BtnServico_Click(object sender, RoutedEventArgs e)
        {
            Hide(); //Esconder atual
            Servico janelaServico = new Servico();
            janelaServico.Show();
        }

        private void BtnCliCadastro_Click(object sender, RoutedEventArgs e)
        {
            Hide(); //Esconder atual
            cadastrarCli janelaCadastrarCli = new cadastrarCli();
            janelaCadastrarCli.Show();
        }

        private void BtnFuncCadastro_Click(object sender, RoutedEventArgs e)
        {
            Hide(); //Esconder atual
            funcCadastro janelaFuncCadastro = new funcCadastro();
            janelaFuncCadastro.Show();
        }

        private void BtnServCadastro_Click(object sender, RoutedEventArgs e)
        {
            Hide(); //Esconder atual
            servicoCadastro janelaServicoCadastro = new servicoCadastro();
            janelaServicoCadastro.Show();
        }
    }
}
