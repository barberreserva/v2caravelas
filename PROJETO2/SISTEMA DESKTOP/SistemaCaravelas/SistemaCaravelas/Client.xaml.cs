﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaCaravelas
{
    /// <summary>
    /// Lógica interna para Client.xaml
    /// </summary>
    public partial class Client : Window
    {
        
        public Client()
        {
            InitializeComponent();
        }

        private void BtnCliAdd_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            cadastrarCli btnCadastro = new cadastrarCli();
            btnCadastro.Show();
        }

        private void Client1_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT * FROM cliente ORDER BY nomeCli";

            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridCliente.DisplayMemberPath = "nomeCli";
            dataGridCliente.ItemsSource = dt.DefaultView;
        }

        private void CmbStatus_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT * FROM cliente WHERE statusCli = @statusCli ORDER BY nomeCli ";

            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            com.Parameters.Clear();
            com.Parameters.Add("@statusCli", MySqlDbType.String).Value = cmbStatus.Text;
            com.CommandType = CommandType.Text; /* Executa o comando */

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridCliente.DisplayMemberPath = "nomeCli";
            dataGridCliente.ItemsSource = dt.DefaultView;
        }

        private void TxtBuscarCliente_TextChanged(object sender, TextChangedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT * FROM cliente WHERE nomeCli = @nomeCli ORDER BY nomeCli ";

            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            com.Parameters.Clear();
            com.Parameters.Add("@nomeCli", MySqlDbType.String).Value = txtBuscarCliente.Text;
            com.CommandType = CommandType.Text; /* Executa o comando */

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridCliente.DisplayMemberPath = "nomeCli";
            dataGridCliente.ItemsSource = dt.DefaultView;
        }

        private void BtnFecharCli_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            menu1 janelaMenu1 = new menu1();
            janelaMenu1.Show();
        }

        private void BtnCliAlterar_Click(object sender, RoutedEventArgs e)
        {
            var rowView = dataGridCliente.SelectedItems[0] as DataRowView;
            Hide();
            cadastrarCli alterar = new cadastrarCli();
            alterar.Show();
            alterar.txtCliCodigo.Text = rowView["idcliente"].ToString();
            alterar.txtCliNome.Text = rowView["nomeCli"].ToString();
            alterar.txtCliEmail.Text = rowView["emailCli"].ToString();
            alterar.txtCliSenha.Text = rowView["senhaCli"].ToString();
            
            if (rowView["statusCli"].ToString() == "ATIVO")
            {
                alterar.ChkStatus.IsChecked = true;
            }
            else
            {
                alterar.ChkStatus.IsChecked = false;
            }

            alterar.dataCadCli.Text = rowView["dataCadCli"].ToString();
            
            alterar.codCli = rowView["idcliente"].ToString();

            //buscar o nome da empresa pelo código que consta na grid
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT nomeEmp FROM empresa WHERE idempresa = ?";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            com.Parameters.Clear();
            com.Parameters.Add("@idempresa", MySqlDbType.String).Value = alterar.codCli;
            com.CommandType = CommandType.Text; /*EXECUTA O COMANDO */
                                                //RECEBE CONTEÙDO DO BANCO
            MySqlDataReader dr = com.ExecuteReader();
            dr.Read();
            
        }
    }
}
